// SPDX-FileCopyrightText: 2022 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qwtapi/model/hudmsg.h"
#include "qwtapi/model/state.h"
#include "qwtapi/model/indicators.h"
#include "qwtapi/service/stateservice.h"
#include "qwtapi/service/hudmsgservice.h"
#include "qwtapi/service/indicatorsservice.h"
#include "qwtapi/service/chatmessagesservice.h"
#include "qwtapi/service/mapinfoservice.h"
#include "qwtapi/service/mapobjsservice.h"
#include "qwtapi/service/missionservice.h"
#include "qwtapi/service/mapimageservice.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    State* m_updateableState;
    HudMsg* m_updateableHudMsg;
    Indicators* m_updateableIndicators;
    ChatMessages* m_updateableChatMessages;
    Mapinfo* m_updateableMapInfo;
    MapObjs* m_updateableMapObjs;
    Mission* m_updateableMission;

    StateService m_stateService;
    HudMsgService m_hudmsgService;
    IndicatorsService m_indicatorsService;
    ChatMessagesService m_chatMessagesService;
    MapinfoService m_mapInfoService;
    MapObjsService m_mapObjsService;
    MissionService m_missionService;
    MapImageService m_mapImageService;

public slots:
    void stateUpdate(State* state);
    void hudMsgUpdate(HudMsg* hudmsg);
    void indicatorsUpdate(Indicators* indicators);
    void chatMessagesUpdate(ChatMessages* messages);
    void mapInfoUpdate(Mapinfo* mapinfo);
    void mapObjsUpdate(MapObjs* objs);
    void missionUpdate(Mission* mission);
    void mapImageUpdate(QPixmap image);
};
#endif // MAINWINDOW_H

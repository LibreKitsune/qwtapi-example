<!--
SPDX-FileCopyrightText: 2022 SinonOE <sinonat@oesterle.dev>

SPDX-License-Identifier: GPL-3.0-only
-->

# qWTapi Example

Small project showcasing the qWTapi

## Description
This project is meant to showcase the [qWTapi](https://gitlab.com/LibreKitsune/qwtapi)

## License
GNU General Public License v3.0 (GPL-3.0-only)

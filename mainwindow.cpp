// SPDX-FileCopyrightText: 2022 SinonOE <sinonat@oesterle.dev>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "qwtapi/controller/hudmsgjsonconverter.h"
#include "qwtapi/controller/statejsonconverter.h"
#include <QListWidgetItem>
#include <QMetaEnum>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle("qWTapi example");

    // Prepare State variables
    m_updateableState = new State(this);
    m_updateableHudMsg = new HudMsg(this);
    m_updateableIndicators = new Indicators(this);
    m_updateableChatMessages = new ChatMessages(this);
    m_updateableMapInfo = new Mapinfo(this);
    m_updateableMapObjs = new MapObjs(this);
    m_updateableMission = new Mission(this);

    // Set polling interval (calls the API every 100 ms)
    int interval = 100;
    m_stateService.setIntervalms(interval);
    m_hudmsgService.setIntervalms(interval);
    m_indicatorsService.setIntervalms(interval);
    m_chatMessagesService.setIntervalms(interval);
    m_mapInfoService.setIntervalms(interval);
    m_mapObjsService.setIntervalms(interval);
    m_missionService.setIntervalms(interval);
    m_mapImageService.setIntervalms(interval);


    // Connect services to update methods
    connect(&m_stateService, &StateService::stateRecieved, this, &MainWindow::stateUpdate);
    connect(&m_hudmsgService, &HudMsgService::hudMsgRecieved, this, &MainWindow::hudMsgUpdate);
    connect(&m_indicatorsService, &IndicatorsService::indicatorsRecieved, this, &MainWindow::indicatorsUpdate);
    connect(&m_chatMessagesService, &ChatMessagesService::chatMessagesRecieved, this, &MainWindow::chatMessagesUpdate);
    connect(&m_mapInfoService, &MapinfoService::mapinfoRecieved, this, &MainWindow::mapInfoUpdate);
    connect(&m_mapObjsService, &MapObjsService::mapObjsRecieved, this, &MainWindow::mapObjsUpdate);
    connect(&m_missionService, &MissionService::missionRecieved, this, &MainWindow::missionUpdate);
    connect(&m_mapImageService, &MapImageService::imageRecieved, this, &MainWindow::mapImageUpdate);


    // connect start stop buttons to services
    connect(ui->pb_start, &QPushButton::clicked, &m_stateService, &StateService::start);
    connect(ui->pb_stop, &QPushButton::clicked, &m_stateService, &StateService::stop);
    connect(ui->pb_start, &QPushButton::clicked, &m_hudmsgService, &HudMsgService::start);
    connect(ui->pb_stop, &QPushButton::clicked, &m_hudmsgService, &HudMsgService::stop);
    connect(ui->pb_start, &QPushButton::clicked, &m_indicatorsService, &IndicatorsService::start);
    connect(ui->pb_stop, &QPushButton::clicked, &m_indicatorsService, &IndicatorsService::stop);
    connect(ui->pb_start, &QPushButton::clicked, &m_chatMessagesService, &ChatMessagesService::start);
    connect(ui->pb_stop, &QPushButton::clicked, &m_chatMessagesService, &ChatMessagesService::stop);
    connect(ui->pb_start, &QPushButton::clicked, &m_mapInfoService, &MapinfoService::start);
    connect(ui->pb_stop, &QPushButton::clicked, &m_mapInfoService, &MapinfoService::stop);
    connect(ui->pb_start, &QPushButton::clicked, &m_mapObjsService, &MapObjsService::start);
    connect(ui->pb_stop, &QPushButton::clicked, &m_mapObjsService, &MapObjsService::stop);
    connect(ui->pb_start, &QPushButton::clicked, &m_missionService, &MissionService::start);
    connect(ui->pb_stop, &QPushButton::clicked, &m_missionService, &MissionService::stop);
    connect(ui->pb_start, &QPushButton::clicked, &m_mapImageService, &MapImageService::start);
    connect(ui->pb_stop, &QPushButton::clicked, &m_mapImageService, &MapImageService::stop);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::stateUpdate(State* state)
{
    // This function prints every single property the State object
    if (state != nullptr) {
        // Update our state variable with new data
        m_updateableState->updateWith(state);
        // delete the leftover data to save RAM (otherwise MemoryLeak)
        delete state;        

        ui->lwstate->clear();

        for(int i = 0; i < m_updateableState->metaObject()->propertyCount(); ++i)
        {
            // Get each property one by one
            QString name = QString::fromUtf8(m_updateableState->metaObject()->property(i).name());
            QVariant value = m_updateableState->metaObject()->property(i).read(m_updateableState);

            // Engine states need to be handled differently since they are QObjects themselfes
            if (name != "engineStates")
            {
                QListWidgetItem* item = new QListWidgetItem(name.append(" ").append(value.typeName()).append(" ").append(value.toString()),ui->lwstate);
                ui->lwstate->addItem(item);
            }
            else
            {
                QList<EngineState*> enginevalues = value.value<QList<EngineState*>>();
                foreach(QObject* obj, enginevalues)
                {
                    for(int i = 0; i < obj->metaObject()->propertyCount(); ++i)
                    {
                        QString name2 = QString::fromUtf8(obj->metaObject()->property(i).name());
                        QVariant value2 = obj->metaObject()->property(i).read(obj);

                        QListWidgetItem* item = new QListWidgetItem(name2 + " " + value2.typeName()+" "+ value2.toString(),ui->lwstate);
                        ui->lwstate->addItem(item);
                    }
                }
            }
        }
    }
}

void MainWindow::hudMsgUpdate(HudMsg* msg)
{

    if (msg != nullptr)
    {
        m_updateableHudMsg->updateWith(msg);
        delete msg;

        ui->lwhudmsg->clear();

        for(int i = 0; i < m_updateableHudMsg->metaObject()->propertyCount(); ++i)
        {
            QVariant value = m_updateableHudMsg->metaObject()->property(i).read(m_updateableHudMsg);


            QList<HudMsgDamage*> dmgvalues = value.value<QList<HudMsgDamage*>>();
            foreach(QObject* obj, dmgvalues)
            {
                for(int i = 0; i < obj->metaObject()->propertyCount(); ++i)
                {
                    QString name2 = QString::fromUtf8(obj->metaObject()->property(i).name());
                    QVariant value2 = obj->metaObject()->property(i).read(obj);

                    QListWidgetItem* item = new QListWidgetItem(name2 + " " + value2.typeName() + " "+ value2.toString(),ui->lwhudmsg);
                    ui->lwhudmsg->addItem(item);
                }
            }

            QList<HudMsgEvent*> eventvalues = value.value<QList<HudMsgEvent*>>();
            foreach(QObject* obj, eventvalues)
            {
                for(int i = 0; i < obj->metaObject()->propertyCount(); ++i)
                {
                    QString name2 = QString::fromUtf8(obj->metaObject()->property(i).name());
                    QVariant value2 = obj->metaObject()->property(i).read(obj);

                    QListWidgetItem* item = new QListWidgetItem(name2 + " " + value2.typeName() +" "+ value2.toString(),ui->lwhudmsg);
                    ui->lwhudmsg->addItem(item);
                }
            }
        }
    }
}

void MainWindow::indicatorsUpdate(Indicators* indicators)
{
    if (indicators != nullptr)
    {
        m_updateableIndicators->updateWith(indicators);
        delete indicators;

        ui->lwindicators->clear();
        for(int i = 0; i < m_updateableIndicators->metaObject()->propertyCount(); ++i)
        {
            QString name = QString::fromUtf8(m_updateableIndicators->metaObject()->property(i).name());
            QVariant value = m_updateableIndicators->metaObject()->property(i).read(m_updateableIndicators);

            if (name != "indicators")
            {
              QListWidgetItem* item = new QListWidgetItem(name.append(" ").append(value.typeName()).append(" ").append(value.toString()),ui->lwindicators);
              ui->lwindicators->addItem(item);
            }
            else
            {
                QMap<QString, QVariant> values = value.toMap();
                foreach(QString key, values.keys())
                {
                  QString val = values.value(key).toString();
                  QListWidgetItem* item = new QListWidgetItem(key + " " + values.value(key).typeName() +" "+ val,ui->lwindicators);
                  ui->lwindicators->addItem(item);
                }
            }
        }
    }
}

void MainWindow::chatMessagesUpdate(ChatMessages* messages)
{
    if (messages != nullptr) {
        m_updateableChatMessages->updateWith(messages);
        delete messages;

        ui->lwchat->clear();

        for(int i = 0; i < m_updateableChatMessages->metaObject()->propertyCount(); ++i)
        {
            QString name = QString::fromUtf8(m_updateableChatMessages->metaObject()->property(i).name());
            QVariant value = m_updateableChatMessages->metaObject()->property(i).read(m_updateableChatMessages);

            QListWidgetItem* item = new QListWidgetItem(name.append(" ").append(value.typeName()).append(" ").append(value.toString()),ui->lwchat);
            ui->lwstate->addItem(item);

            QList<ChatMessage*> chatMessages = value.value<QList<ChatMessage*>>();
            foreach(QObject* obj, chatMessages)
            {
                for(int i = 0; i < obj->metaObject()->propertyCount(); ++i)
                {
                    QString name2 = QString::fromUtf8(obj->metaObject()->property(i).name());
                    QVariant value2 = obj->metaObject()->property(i).read(obj);

                    QListWidgetItem* item = new QListWidgetItem(ui->lwchat);
                    // label fuckery to get colour
                    QLabel *lbl = new QLabel();
                    lbl->setParent(ui->lwchat);
                    lbl->setText(name2 + " " +value2.typeName()+" "+ value2.toString());
                    ui->lwchat->addItem(item);
                    ui->lwchat->setItemWidget(item,lbl);
                }
            }
        }
    }
}

void MainWindow::mapInfoUpdate(Mapinfo* mapinfo)
{
    if (mapinfo != nullptr) {
        m_updateableMapInfo->updateWith(mapinfo);
        delete mapinfo;

        ui->lwmapinfo->clear();

        for(int i = 0; i < m_updateableMapInfo->metaObject()->propertyCount(); ++i)
        {
            QString name = QString::fromUtf8(m_updateableMapInfo->metaObject()->property(i).name());
            QVariant value = m_updateableMapInfo->metaObject()->property(i).read(m_updateableMapInfo);

            QListWidgetItem* item = new QListWidgetItem(name.append(" ").append(value.typeName()).append(" ").append(value.toString()),ui->lwmapinfo);
            ui->lwmapinfo->addItem(item);
        }
    }
}

void MainWindow::mapObjsUpdate(MapObjs* objs)
{
    if (objs != nullptr) {
        m_updateableMapObjs->updateWith(objs);
        delete objs;

        ui->lwmapobj->clear();

        for(int i = 0; i < m_updateableMapObjs->metaObject()->propertyCount(); ++i)
        {
            QString name = QString::fromUtf8(m_updateableMapObjs->metaObject()->property(i).name());
            QVariant value = m_updateableMapObjs->metaObject()->property(i).read(m_updateableMapObjs);

            QListWidgetItem* item = new QListWidgetItem(name.append(" ").append(value.typeName()).append(" ").append(value.toString()),ui->lwmapobj);
            ui->lwmapobj->addItem(item);

            QList<MapObj*> mapobjs = value.value<QList<MapObj*>>();
            foreach(QObject* obj, mapobjs)
            {
                for(int i = 0; i < obj->metaObject()->propertyCount(); ++i)
                {
                    QString name2 = QString::fromUtf8(obj->metaObject()->property(i).name());
                    QVariant value2 = obj->metaObject()->property(i).read(obj);

                    QListWidgetItem* item = new QListWidgetItem(name2 + " " +value2.typeName()+" "+ value2.toString(),ui->lwmapobj);
                    ui->lwmapobj->addItem(item);

                }
            }
        }
    }
}

void MainWindow::missionUpdate(Mission* mission)
{
    if (mission != nullptr)
    {
        m_updateableMission->updateWith(mission);
        delete mission;

        ui->lwmission->clear();

        for(int i = 0; i < m_updateableMission->metaObject()->propertyCount(); ++i)
        {
            QVariant value = m_updateableMission->metaObject()->property(i).read(m_updateableMission);
            QString name = QString::fromUtf8(m_updateableMission->metaObject()->property(i).name());

            QListWidgetItem* item = new QListWidgetItem(name.append(" ").append(value.typeName()).append(" ").append(value.toString()),ui->lwmission);
            ui->lwmission->addItem(item);

            QList<Objective*> objectives = value.value<QList<Objective*>>();
            foreach(QObject* obj, objectives)
            {
                for(int i = 0; i < obj->metaObject()->propertyCount(); ++i)
                {
                    QString name2 = QString::fromUtf8(obj->metaObject()->property(i).name());
                    QVariant value2 = obj->metaObject()->property(i).read(obj);

                    QListWidgetItem* item = new QListWidgetItem(ui->lwmission);
                    // label fuckery to get colour
                    QLabel *lbl = new QLabel();
                    lbl->setParent(ui->lwmission);
                    lbl->setText(name2 + " " +value2.typeName()+" "+ value2.toString());
                    ui->lwmission->addItem(item);
                    ui->lwmission->setItemWidget(item,lbl);
                }
            }
        }
    }
}

void MainWindow::mapImageUpdate(QPixmap image)
{
    QSize size = ui->lblmapimage->size();

    // Make image slightly smaller than the label.
    // Otherwise we might observe the label to be slowly growing
    size.setHeight(size.height()-5);
    size.setWidth(size.width()-5);
    QPixmap img = image.scaled(size, Qt::AspectRatioMode::KeepAspectRatio);

    ui->lblmapimage->setPixmap(img);
}
